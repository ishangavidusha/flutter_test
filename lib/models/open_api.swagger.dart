import 'open_api.models.swagger.dart';
import 'package:chopper/chopper.dart';
import 'dart:convert';

import 'client_mapping.dart';
import 'package:chopper/chopper.dart' as chopper;
export 'open_api.models.swagger.dart';

part 'open_api.swagger.chopper.dart';

// **************************************************************************
// SwaggerChopperGenerator
// **************************************************************************

@ChopperApi()
abstract class OpenApi extends ChopperService {
  static OpenApi create(
      {ChopperClient? client,
      Authenticator? authenticator,
      String? baseUrl,
      Iterable<dynamic>? interceptors}) {
    if (client != null) {
      return _$OpenApi(client);
    }

    final newClient = ChopperClient(
        services: [_$OpenApi()],
        converter: $JsonSerializableConverter(),
        interceptors: interceptors ?? [],
        authenticator: authenticator,
        baseUrl: baseUrl ?? 'https://emedia.ishangavidusha.com');
    return _$OpenApi(newClient);
  }

  ///Root
  Future<chopper.Response<EmediaApi>> $Get() {
    generatedMapping.putIfAbsent(EmediaApi, () => EmediaApi.fromJsonFactory);

    return _$Get();
  }

  ///Root
  @Get(path: '/')
  Future<chopper.Response<EmediaApi>> _$Get();

  ///Ping
  Future<chopper.Response<Ping>> pingGet() {
    generatedMapping.putIfAbsent(Ping, () => Ping.fromJsonFactory);

    return _pingGet();
  }

  ///Ping
  @Get(path: '/ping')
  Future<chopper.Response<Ping>> _pingGet();

  ///Get Posts
  Future<chopper.Response<List<Posts>>> postsGet() {
    generatedMapping.putIfAbsent(Posts, () => Posts.fromJsonFactory);

    return _postsGet();
  }

  ///Get Posts
  @Get(path: '/posts')
  Future<chopper.Response<List<Posts>>> _postsGet();

  ///Create Post
  Future<chopper.Response<Posts>> postsPost({required Posts? body}) {
    generatedMapping.putIfAbsent(Posts, () => Posts.fromJsonFactory);

    return _postsPost(body: body);
  }

  ///Create Post
  @Post(path: '/posts')
  Future<chopper.Response<Posts>> _postsPost({@Body() required Posts? body});

  ///Get Post
  ///@param id
  Future<chopper.Response<Posts>> postsIdGet({required int? id}) {
    generatedMapping.putIfAbsent(Posts, () => Posts.fromJsonFactory);

    return _postsIdGet(id: id);
  }

  ///Get Post
  ///@param id
  @Get(path: '/posts/{id}')
  Future<chopper.Response<Posts>> _postsIdGet({@Path('id') required int? id});

  ///Update Post
  ///@param id
  Future<chopper.Response<Posts>> postsIdPut(
      {required int? id, required Posts? body}) {
    generatedMapping.putIfAbsent(Posts, () => Posts.fromJsonFactory);

    return _postsIdPut(id: id, body: body);
  }

  ///Update Post
  ///@param id
  @Put(path: '/posts/{id}')
  Future<chopper.Response<Posts>> _postsIdPut(
      {@Path('id') required int? id, @Body() required Posts? body});

  ///Delete Post
  ///@param id
  Future<chopper.Response<Posts>> postsIdDelete({required int? id}) {
    generatedMapping.putIfAbsent(Posts, () => Posts.fromJsonFactory);

    return _postsIdDelete(id: id);
  }

  ///Delete Post
  ///@param id
  @Delete(path: '/posts/{id}')
  Future<chopper.Response<Posts>> _postsIdDelete(
      {@Path('id') required int? id});

  ///Get Users
  Future<chopper.Response<List<User>>> usersGet() {
    generatedMapping.putIfAbsent(User, () => User.fromJsonFactory);

    return _usersGet();
  }

  ///Get Users
  @Get(path: '/users')
  Future<chopper.Response<List<User>>> _usersGet();

  ///Create User
  Future<chopper.Response<User>> usersPost({required User? body}) {
    generatedMapping.putIfAbsent(User, () => User.fromJsonFactory);

    return _usersPost(body: body);
  }

  ///Create User
  @Post(path: '/users')
  Future<chopper.Response<User>> _usersPost({@Body() required User? body});

  ///Get User
  ///@param id
  Future<chopper.Response<User>> usersIdGet({required int? id}) {
    generatedMapping.putIfAbsent(User, () => User.fromJsonFactory);

    return _usersIdGet(id: id);
  }

  ///Get User
  ///@param id
  @Get(path: '/users/{id}')
  Future<chopper.Response<User>> _usersIdGet({@Path('id') required int? id});

  ///Update User
  ///@param id
  Future<chopper.Response<User>> usersIdPut(
      {required int? id, required User? body}) {
    generatedMapping.putIfAbsent(User, () => User.fromJsonFactory);

    return _usersIdPut(id: id, body: body);
  }

  ///Update User
  ///@param id
  @Put(path: '/users/{id}')
  Future<chopper.Response<User>> _usersIdPut(
      {@Path('id') required int? id, @Body() required User? body});

  ///Delete User
  ///@param id
  Future<chopper.Response<User>> usersIdDelete({required int? id}) {
    generatedMapping.putIfAbsent(User, () => User.fromJsonFactory);

    return _usersIdDelete(id: id);
  }

  ///Delete User
  ///@param id
  @Delete(path: '/users/{id}')
  Future<chopper.Response<User>> _usersIdDelete({@Path('id') required int? id});
}

typedef $JsonFactory<T> = T Function(Map<String, dynamic> json);

class $CustomJsonDecoder {
  $CustomJsonDecoder(this.factories);

  final Map<Type, $JsonFactory> factories;

  dynamic decode<T>(dynamic entity) {
    if (entity is Iterable) {
      return _decodeList<T>(entity);
    }

    if (entity is T) {
      return entity;
    }

    if (entity is Map<String, dynamic>) {
      return _decodeMap<T>(entity);
    }

    return entity;
  }

  T _decodeMap<T>(Map<String, dynamic> values) {
    final jsonFactory = factories[T];
    if (jsonFactory == null || jsonFactory is! $JsonFactory<T>) {
      return throw "Could not find factory for type $T. Is '$T: $T.fromJsonFactory' included in the CustomJsonDecoder instance creation in bootstrapper.dart?";
    }

    return jsonFactory(values);
  }

  List<T> _decodeList<T>(Iterable values) =>
      values.where((v) => v != null).map<T>((v) => decode<T>(v) as T).toList();
}

class $JsonSerializableConverter extends chopper.JsonConverter {
  @override
  chopper.Response<ResultType> convertResponse<ResultType, Item>(
      chopper.Response response) {
    if (response.bodyString.isEmpty) {
      // In rare cases, when let's say 204 (no content) is returned -
      // we cannot decode the missing json with the result type specified
      return chopper.Response(response.base, null, error: response.error);
    }

    final jsonRes = super.convertResponse(response);
    return jsonRes.copyWith<ResultType>(
        body: $jsonDecoder.decode<Item>(jsonRes.body) as ResultType);
  }
}

final $jsonDecoder = $CustomJsonDecoder(generatedMapping);
