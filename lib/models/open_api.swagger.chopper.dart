// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'open_api.swagger.dart';

// **************************************************************************
// ChopperGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line, always_specify_types, prefer_const_declarations, unnecessary_brace_in_string_interps
class _$OpenApi extends OpenApi {
  _$OpenApi([ChopperClient? client]) {
    if (client == null) return;
    this.client = client;
  }

  @override
  final definitionType = OpenApi;

  @override
  Future<Response<EmediaApi>> _$Get() {
    final $url = '/';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<EmediaApi, EmediaApi>($request);
  }

  @override
  Future<Response<Ping>> _pingGet() {
    final $url = '/ping';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<Ping, Ping>($request);
  }

  @override
  Future<Response<List<Posts>>> _postsGet() {
    final $url = '/posts';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<List<Posts>, Posts>($request);
  }

  @override
  Future<Response<Posts>> _postsPost({required Posts? body}) {
    final $url = '/posts';
    final $body = body;
    final $request = Request('POST', $url, client.baseUrl, body: $body);
    return client.send<Posts, Posts>($request);
  }

  @override
  Future<Response<Posts>> _postsIdGet({required int? id}) {
    final $url = '/posts/${id}';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<Posts, Posts>($request);
  }

  @override
  Future<Response<Posts>> _postsIdPut(
      {required int? id, required Posts? body}) {
    final $url = '/posts/${id}';
    final $body = body;
    final $request = Request('PUT', $url, client.baseUrl, body: $body);
    return client.send<Posts, Posts>($request);
  }

  @override
  Future<Response<Posts>> _postsIdDelete({required int? id}) {
    final $url = '/posts/${id}';
    final $request = Request('DELETE', $url, client.baseUrl);
    return client.send<Posts, Posts>($request);
  }

  @override
  Future<Response<List<User>>> _usersGet() {
    final $url = '/users';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<List<User>, User>($request);
  }

  @override
  Future<Response<User>> _usersPost({required User? body}) {
    final $url = '/users';
    final $body = body;
    final $request = Request('POST', $url, client.baseUrl, body: $body);
    return client.send<User, User>($request);
  }

  @override
  Future<Response<User>> _usersIdGet({required int? id}) {
    final $url = '/users/${id}';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<User, User>($request);
  }

  @override
  Future<Response<User>> _usersIdPut({required int? id, required User? body}) {
    final $url = '/users/${id}';
    final $body = body;
    final $request = Request('PUT', $url, client.baseUrl, body: $body);
    return client.send<User, User>($request);
  }

  @override
  Future<Response<User>> _usersIdDelete({required int? id}) {
    final $url = '/users/${id}';
    final $request = Request('DELETE', $url, client.baseUrl);
    return client.send<User, User>($request);
  }
}
