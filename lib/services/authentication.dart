import 'dart:async';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

String _TOKEN_KEY = "25U8BFKYAJ34FW9SUMPV44BUHZRNJLC9";

class AuthService with ChangeNotifier {
  static AuthService? _instance;
  late final SharedPreferences sharedPreferences;
  final StreamController<bool> _streamController = StreamController.broadcast();
  String? _token;

  String? get token => _token;
  Stream<bool> get onAuthStateChanged => _streamController.stream;

  AuthService._(this.sharedPreferences);

  factory AuthService.initialize(SharedPreferences sharedPreferences) {
    if (_instance == null) {
      _instance = AuthService._(sharedPreferences);
    } else {
      throw Exception("AuthService already initialized");
    }
    return _instance!;
  }

  static AuthService get instance {
    if (_instance == null) {
      throw Exception("AuthService not initialized");
    }
    return _instance!;
  }

  Future<String?> login(String token) async {
    try {
      await sharedPreferences.setString(_TOKEN_KEY, token);
      _token = token;
      _streamController.add(true);
      notifyListeners();
      return null;
    } catch (e) {
      return e.toString();
    }
  }

  Future<String?> logout() async {
    try {
      await sharedPreferences.remove(_TOKEN_KEY);
      _token = null;
      _streamController.add(false);
      notifyListeners();
      return null;
    } catch (e) {
      return e.toString();
    }
  }

  String? checkLoginState() {
    _token = sharedPreferences.getString(_TOKEN_KEY);
    if (_token != null) {
      _streamController.add(true);
    } else {
      _streamController.add(false);
    }
    notifyListeners();
    return _token;
  }
}