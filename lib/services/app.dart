import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

// ignore: non_constant_identifier_names
String _ONBOARD_KEY = "VXK3B2EH3H5UP9WX3A7M53W9F8FPR447";

class AppService with ChangeNotifier {
  late final SharedPreferences sharedPreferences;
  bool _loginState = false;
  bool _initialized = false;
  bool _onboarding = false;
  AppService({
    required this.sharedPreferences,
  });

  bool get loginState => _loginState;
  bool get initialized => _initialized;
  bool get onboarding => _onboarding;

  set loginState(bool state) {
    _loginState = state;
    _initialized = false;
    notifyListeners();
  }

  set onboarding(bool value) {
    sharedPreferences.setBool(_ONBOARD_KEY, value);
    _onboarding = value;
    notifyListeners();
  }

  Future<bool> startUpCheck() async {
    _getOnBoard();
    await Future.delayed(const Duration(seconds: 1));
    _initialized = true;
    notifyListeners();
    return true;
  }

  void _getOnBoard() {
    if (sharedPreferences.containsKey(_ONBOARD_KEY)) {
      _onboarding = sharedPreferences.getBool(_ONBOARD_KEY) ?? false;
    }
  }
}