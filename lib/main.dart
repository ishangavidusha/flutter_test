import 'dart:async';

import 'package:emedia_flutter/routes/router.dart';
import 'package:emedia_flutter/services/app.dart';
import 'package:emedia_flutter/services/authentication.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

GlobalKey<ScaffoldMessengerState> globalMessengerKey = GlobalKey();
Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  AuthService.initialize(sharedPreferences);
  runApp(EmediaFlutter(sharedPreferences: sharedPreferences));
}

class EmediaFlutter extends StatefulWidget {
  final SharedPreferences sharedPreferences;
  const EmediaFlutter({
    Key? key,
    required this.sharedPreferences,
  }) : super(key: key);

  @override
  State<EmediaFlutter> createState() => _EmediaFlutterState();
}

class _EmediaFlutterState extends State<EmediaFlutter> {
  late AuthService _authService;
  late AppService _appService;
  late StreamSubscription<bool> _streamSubscription;

  @override
  void initState() {
    _appService = AppService(sharedPreferences: widget.sharedPreferences);
    _authService = AuthService.instance;
    _streamSubscription = _authService.onAuthStateChanged.listen(authStateChanged);
    super.initState();
  }

  void authStateChanged(bool state) {
    if (kDebugMode) {
      print("AuthStateChanged: $state");
    }
    _appService.loginState = state;
  }

  @override
  void dispose() {
    _streamSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<AppService>(create: (_) => _appService),
        ChangeNotifierProvider<AuthService>(create: (_) => _authService),
        Provider<AppRouter>(create: (_) => AppRouter(_appService)),
      ],
      child: Builder(
        builder: (context) {
          final GoRouter router = Provider.of<AppRouter>(context, listen: false).router;
          return MaterialApp.router(
            title: "Emedia Flutter",
            debugShowCheckedModeBanner: false,
            theme: ThemeData(
              colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple)
            ),
            routerDelegate: router.routerDelegate,
            routeInformationParser: router.routeInformationParser,
            scaffoldMessengerKey: globalMessengerKey,
          );
        }
      ),
    );
  }
}
