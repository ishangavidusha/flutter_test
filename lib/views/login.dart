import 'package:emedia_flutter/services/authentication.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../services/app.dart';

class LogInPage extends StatefulWidget {
  const LogInPage({ Key? key }) : super(key: key);

  @override
  State<LogInPage> createState() => _LogInPageState();
}

class _LogInPageState extends State<LogInPage> {
  String _token = '';
  @override
  Widget build(BuildContext context) {
    final authService = Provider.of<AuthService>(context);
    return Scaffold(
      body: Stack(
        children: [
          Positioned.fill(
            child: Center(
              child: SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text("Login Page", style: Theme.of(context).textTheme.headline4),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: SizedBox(
                        width: MediaQuery.of(context).size.width * 0.5,
                        child: TextField(
                          decoration: const InputDecoration(
                            labelText: "Token",
                          ),
                          onChanged: (value) {
                            setState(() {
                              _token = value;
                            });
                          },
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ElevatedButton(
                        child: Text("Login", style: Theme.of(context).textTheme.bodyMedium),
                        onPressed: () {
                          if (_token.isNotEmpty) {
                            authService.login(_token).then((value) {
                              if (value == null) {
                                ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                                  content: Text("Login Successful"),
                                ));
                              } else {
                                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                  content: Text(value),
                                ));
                              }
                            });
                          }
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}