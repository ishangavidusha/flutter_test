import 'package:emedia_flutter/services/app.dart';
import 'package:emedia_flutter/services/authentication.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({ Key? key }) : super(key: key);

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  late AppService _appService;


  @override
  void initState() {
    _appService = Provider.of<AppService>(context, listen: false);
    WidgetsBinding.instance!.addPostFrameCallback(((timeStamp) => onStart()));
    super.initState();
  }

  void onStart() {
    AuthService.instance.checkLoginState();
    _appService.startUpCheck();
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(
        child: Text("Splash Page"),
      ),
    );
  }
}