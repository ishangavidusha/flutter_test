import 'dart:async';
import 'dart:ui';

import 'package:chopper/chopper.dart';
import 'package:emedia_flutter/models/client_index.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../models/open_api.models.swagger.dart';
import '../services/app.dart';
import '../services/authentication.dart';

class HomePage extends StatefulWidget {
  const HomePage({
    Key? key,
  }) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Stopwatch stopwatch = Stopwatch();
  late OpenApi _openApi;
  late Future<Response<List<Posts>>> resolveFuture;
  late Duration executionTime;

  @override
  void initState() {
    _openApi = OpenApi.create(baseUrl: "https://emedia.ishangavidusha.com");
    executionTime = const Duration(milliseconds: 0);
    resolveFuture = getPosts();
    super.initState();
  }

  Future<Response<List<Posts>>> getPosts() async {
    stopwatch.reset();
    stopwatch.start();
    Response<List<Posts>> response = await _openApi.postsGet();
    stopwatch.stop();
    setState(() {
      executionTime = Duration(milliseconds: stopwatch.elapsedMilliseconds);
    });
    return response;
  }

  void shouldReload() => setState(() {
    resolveFuture = getPosts();
  });

  @override
  Widget build(BuildContext context) {
    final appService = Provider.of<AppService>(context);
    final authService = Provider.of<AuthService>(context);
    return Scaffold(
      appBar: AppBar(
        title: const Text("Emedia Open API"),
        actions: [
          IconButton(onPressed: (() => shouldReload()), icon: const Icon(Icons.replay_rounded)),
          IconButton(onPressed: (() => authService.logout()), icon: const Icon(Icons.logout_rounded)),
        ],
      ),
      body: Stack(
        children: [
          Positioned.fill(
            child: FutureBuilder(
              future: resolveFuture,
              builder: (BuildContext context, AsyncSnapshot<Response<List<Posts>>> snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                } else if (snapshot.hasError || !snapshot.hasData) {
                  return Center(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(snapshot.error?.toString() ?? "No Data"),
                    ),
                  );
                } else {
                  Response<List<Posts>>? response = snapshot.data;
                  if (response?.isSuccessful ?? false) {
                    return ListView.builder(
                      itemCount: response?.body?.length ?? 0,
                      itemBuilder: (BuildContext context, int index) {
                        return ListTile(
                          title: Text(response?.body?[index].title ?? ""),
                          subtitle: Text(response?.body?[index].body ?? "", maxLines: 2),
                        );
                      },
                    );
                  } else {
                    return Center(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(response?.error.toString() ?? "No Data"),
                      ),
                    );
                  }
                }
              },
            ),
          ),
          Positioned(
            bottom: 0,
            width: MediaQuery.of(context).size.width,
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 8),
              color: Colors.deepPurple.withAlpha(220),
              child: Text(
                "Last execution time: ${executionTime.inMilliseconds} ms.",
                style: const TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 14,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}