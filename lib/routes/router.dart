import 'package:emedia_flutter/routes/pages.dart';
import 'package:emedia_flutter/services/app.dart';
import 'package:emedia_flutter/views/error.dart';
import 'package:emedia_flutter/views/login.dart';
import 'package:emedia_flutter/views/onboard.dart';
import 'package:emedia_flutter/views/splash.dart';
import 'package:go_router/go_router.dart';

import '../views/home.dart';

class AppRouter {
  final AppService _appService;

  AppRouter(this._appService);

  GoRouter get router => _goRouter;

  late final GoRouter _goRouter = GoRouter(
    refreshListenable: _appService,
    initialLocation: Pages.home.toPath(),
    routes: [
      GoRoute(
        path: Pages.home.toPath(),
        name: Pages.home.toPathName(),
        pageBuilder: (context, state) => NoTransitionPage(key: state.pageKey, child: const HomePage()),
      ),
      GoRoute(
        path: Pages.onboarding.toPath(),
        name: Pages.onboarding.toPathName(),
        pageBuilder: (context, state) => NoTransitionPage(key: state.pageKey, child: const OnboardPage()),
      ),
      GoRoute(
        path: Pages.splash.toPath(),
        name: Pages.splash.toPathName(),
        pageBuilder: (context, state) => NoTransitionPage(key: state.pageKey, child: const SplashPage()),
      ),
      GoRoute(
        path: Pages.login.toPath(),
        name: Pages.login.toPathName(),
        pageBuilder: (context, state) => NoTransitionPage(key: state.pageKey, child: const LogInPage()),
      ),
      GoRoute(
        path: Pages.error.toPath(),
        name: Pages.error.toPathName(),
        pageBuilder: (context, state) => NoTransitionPage(key: state.pageKey, child: ErrorPage(error: state.extra.toString())),
      ),
    ],
    errorPageBuilder: (context, state) => NoTransitionPage(key: state.pageKey, child: ErrorPage(error: state.error.toString())),
    redirect: (GoRouterState state) {
      final loginLocation = state.namedLocation(Pages.login.toPathName());
      final homeLocation = state.namedLocation(Pages.home.toPathName());
      final splashLocation = state.namedLocation(Pages.splash.toPathName());
      final onboardLocation = state.namedLocation(Pages.onboarding.toPathName());

      final isLogin = _appService.loginState;
      final isInitialized = _appService.initialized;
      final isOnboard = _appService.onboarding;

      final isGoingToLogin = state.subloc == loginLocation;
      final isGoingToInit = state.subloc == splashLocation;
      final isGoingToOnboard = state.subloc == onboardLocation;

      
       // If not Initialized and not going to Initialized redirect to Splash
      if (!isInitialized && !isGoingToInit) {
        return splashLocation;
      } else if (isInitialized && !isOnboard && !isGoingToOnboard) {
        return onboardLocation;
      // If not login and not going to login redirect to Login
      } else if (isInitialized && isOnboard && !isLogin && !isGoingToLogin) {
        return loginLocation;
      } else if ((isLogin && isGoingToLogin) || (isInitialized && isGoingToInit) || (isOnboard && isGoingToOnboard)) {
        return homeLocation;
      } else {
        return null;
      }
    }
  );
}