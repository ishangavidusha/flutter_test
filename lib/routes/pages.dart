import '../utils/packages/enum_to_string.dart';

enum Pages {
  login,
  splash,
  home,
  error,
  onboarding,
}

extension PageExtension on Pages {
  String toPath({bool subRoute = false}) {
    if (this == Pages.home) {
      return subRoute ? "" : "/";
    }
    return subRoute ? EnumToString.convertToString(this).toLowerCase() : "/" + EnumToString.convertToString(this).toLowerCase();
  }

  String toPathName() {
    return EnumToString.convertToString(this).toUpperCase();
  }
}